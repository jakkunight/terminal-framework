# Terminal Framework: A simple framework to build terminal-based applications.

Built on the top of Rust, this library aims to offer a simple cross-platform set 
of utilities to build terminal-based applications without too much effort. Meant to 
be used by programmers who need to style their applications or simply write some 
simple tools.

Currently only Linux platforms are supported, due to my lack of time. But planning to 
support Windows and MacOS as soon as I can.

## Why?

If you've been using Rust for a while, you noticed that there are no truly cross-platform 
sulutions for writting terminal-based apps. Moreover, there are few that supports TTY raw 
mode to handle keyboard events asynchronously. I wanted to create a solution which can 
serve as a base for other tools and utilities I plan to make.

## Features

- Simplicity
- Memory safety
- Cross-platform (comming soon)

## Install

To add this package to your Rust project, simply add it to your `Cargo.toml` file:


```toml
[dependencies]
terminal-framework = "0.0.3"
```

## Contributing

If you like this project, please open an issue at my [GitLab repo]("https://gitlab.com/jakkunight/terminal-framework"). 
I will be glad to hear your suggestions and tips for this project.

