//! This module manages the output on the screen and provides an 
//! abstraction layer to compose a nice terminal user interface.
//!
//! Currently only ANSI terminals are supported.

pub mod colors;
pub mod components;
pub mod styles;
pub mod clear;
pub mod displays;
pub mod cursor;
