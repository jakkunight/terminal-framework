//! As the name suggests, this module manages the screen "clear" 
//! functions. Due to the support for the displays, it also 
//! includes display's specific routines to clear them.
//!
//! Only ANSI compatible clearing modes are supported.

use std::io::{self, Write};

pub trait Clear {
    //! This trait contains the functions for clearing the display.

    fn clear_to_space(&self) {
        //! Erases the current character using a whitespace.
    }

    fn clear_display(&self) {
        //! Clears the current display
    }

    fn clear_display_line(&self) {
        //! Clears the current line on the display.
    }
}

/// This struct provides an implementation of the 
/// trait Clear for the entrie screen.
pub struct Screen {}

impl Screen {
    pub fn new() -> Self {
        //! Returns a new instance of the Screen object.
        Screen {}
    }
}

impl Clear for Screen {

    fn clear_display(&self) {
        //! Clears the entrie screen and moves the cursor to the 
        //! line 0, column 0.
        
        print!("\x1b[2J\x1b[H");
        io::stdout().flush().unwrap();
    }

    fn clear_display_line(&self) {
        //! Clears the entrie line and moves the cursor to the 
        //! begining of the line.
        
        print!("\x1b[2K\x1b[0G");
        io::stdout().flush().unwrap();
    }

    fn clear_to_space(&self) {
        //! Erases the current character using a whitespace.

        print!(" \x1b[1D");
        io::stdout().flush().unwrap();
    }
}



