//! This module holds the screen cursor management. 
//! This is useful, to perform multi-display rendering 
//! and other interesting features, like UI component 
//! rendering (such as buttons, menus, bars, etc).
//!
//! The cursor motions are only available on ANSI terminals, 
//! due to their implementation of ANSI Escape Codes.
//! Support for Windows is comming soon.



