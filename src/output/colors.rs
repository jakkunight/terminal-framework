//! This module aims to provide a set of traits and utils to 
//! get the terminal output colored.
//!
//! Currently only 8-bit ANSI colors are supported.
