//! This module provides control over the input devices, 
//! such as the keyboard and mouse.
//!
//! Currently only the keyboard is supported in "console"
//! mode. "Raw" mode is planned to be supported.

pub mod keyboard;
pub mod mouse;
