//! This module adds support for keyboard input.
//!
//! Currently only prompted mode is supported.
//! The "Raw mode" is not supported, but is planned 
//! to be supported.


pub mod raw;
pub mod buffered;

