//! This module implements simple way to handle buffered
//! input.

use std::io::{self, Write};

pub fn input<T: std::str::FromStr>(prompt: Option<String>) -> T {
    //! This function takes an optional prompt to show to the user.
    //! Returns a T type data if success. If any error occurs, it will
    //! keep asking to the user for a valid input.

    let prompt = match prompt {
        Some(s) => s,
        None => String::new(),
    };

    loop {
        let mut input: String = String::new();
        print!("{} ", prompt);
        io::stdout().flush().unwrap();
        io::stdin().read_line(&mut input).unwrap();
        let input: T = match input.trim().parse() {
            Ok(t) => t,
            Err(_e) => {
                println!("Parse Error");
                print!("Press ENTER to continue...");
                io::stdout().flush().unwrap();
                io::stdin().read_line(&mut input).unwrap();
                for _ in 0..3 {
                    print!("\x1b[1A\x1b[2K");
                }
                io::stdout().flush().unwrap();
                continue;
            }
        };
        return input;
    }
}

pub fn pause() {
    //! Pause the program execution until the user hits 
    //! the ENTER key.

    let mut input: String = String::new();
    print!("Press ENTER to continue...");
    io::stdout().flush().unwrap();
    io::stdin().read_line(&mut input).unwrap();
}
