//! Built on the top of Rust, this library aims to offer a simple cross-platform set 
//! of utilities to build terminal-based applications without too much effort. Meant to 
//! be used by programmers who need to style their applications or simply write some 
//! simple tools.
//!
//! Currently only Linux platforms are supported, due to my lack of time. But planning to 
//! support Windows and MacOS as soon as I can.

#![warn(missing_docs)]

pub mod input;
pub mod output;

