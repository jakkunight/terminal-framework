use terminal_framework::input::keyboard::buffered::{input, pause};
use terminal_framework::output::clear::{Clear, Screen};

fn main() {
    input::<i32>(Some(String::from("Insert an integer:")));
    let screen = Screen::new();
    screen.clear_display();
    pause();
}
